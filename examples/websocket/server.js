var fs = require("fs");
var http = require("http");
var WebSocketServer = require("websocket").server;

// Note: this is just an example program and does not remotely address using this securely.

const { PromiseRPCProtocol } = require("../../promise.rpc.js");

// simple in-memory "database";
const database = {
  tasks: []
};

const clients = {};

function dispatchUpdates(ignoreId) {
  Object.values(clients)
    .filter(client => client.clientId !== ignoreId)
    .forEach(client => {
      client.execute('update', [database.tasks]);
    });
}

function handleClient(connection) {
  // on each connection we create our PromiseRPCProtocol, which will house the shared functions
  const clientId = PromiseRPCProtocol.prototype.randomId();

  let apiServer = new PromiseRPCProtocol({
    createTask(label) {
      const id = PromiseRPCProtocol.prototype.randomId();
      database.tasks.push({ id, label, completed: false });
      dispatchUpdates(clientId);
      return database.tasks;
    },
    updateTask(id, data) {
      const task = database.tasks.find((task) => task.id == id);
      if (task) {
        delete data.id;
        Object.assign(task, data);
        dispatchUpdates(clientId);
      }      
      return database.tasks;
    },
    deleteTask(id) {
      database.tasks = database.tasks.filter((task) => task.id !== id);
      dispatchUpdates(clientId);
      return database.tasks;      
    },
    getTasks() {
      return database.tasks;
    }
  });

  apiServer.clientId = clientId;

  // We need to connect the rpc output -> websocket output for the rpc server protocol
  apiServer.onDispatch = (message) => {
    connection.sendUTF(JSON.stringify(message))
  };

  // We need to connect websocket input -> rpc input for the rpc server protocol
  connection.on("message", function (message) {
    if (message.type === "utf8") {
      apiServer.dispatch(JSON.parse(message.utf8Data));
    }
  });

  // if the connection is destroyed, we destroy the apiServer to shut down all callbacks and timeouts
  connection.on("close", function (reasonCode, description) {
    delete clients[clientId];
    apiServer.destroy();
    console.log(
      new Date() + " Peer " + connection.remoteAddress + " disconnected."
    );
  });

  clients[clientId] = apiServer;
}

/*******************************
 *  HTTP Server
 *******************************/

// create a basic http server
var server = http.createServer(function (req, res) {
  console.log(new Date() + " Received request for " + req.url);
  if (req.url === "/") {
    res.writeHead(200, { "Content-Type": "text/html" });
    res.end(fs.readFileSync("./index.html"));
  } else if (req.url === "/promise.rpc.dist.min.js") {
    res.writeHead(200, { "Content-Type": "application/javascript" });
    res.end(fs.readFileSync("../../promise.rpc.dist.min.js"));
  } else {
    res.writeHead(404);
    res.end();
  }
});

// listen on 3005
server.listen(3005, function () {
  console.log(new Date() + " Server is listening on port 3005");
});


/*******************************
 *  Websocket Server
 *******************************/

// create the basic websocket
var wsServer = new WebSocketServer({
  httpServer: server,
  autoAcceptConnections: false,
});


// when a connection is requested
wsServer.on("request", function (request) {
  // in production you should test requst.origin goto websocket server's documentation for more info.  
  let connection = request.accept('api', request.origin);
  console.log(`${new Date()} Connection accepted`);
  
  // create new rpc channel
  handleClient(connection);
});

