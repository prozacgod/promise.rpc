"use strict";
var Duplex = require("readable-stream").Duplex;
var PromiseRPCProtocol = require("./promise.rpc.protocol").PromiseRPCProtocol;

function PromiseRPCStream(handler) {
  var self = this;

  Duplex.call(self, { objectMode: true });
  PromiseRPCProtocol.call(self, handler);

  self.onDispatch = function(msg) {
    self.push(msg);
  };
}

PromiseRPCStream.prototype = Object.create(Duplex.prototype);
Object.assign(PromiseRPCStream.prototype, PromiseRPCProtocol.prototype);
PromiseRPCProtocol.prototype.constructor = PromiseRPCProtocol;

PromiseRPCStream.prototype._read = function() {
  // https://khanna.cc/blog/readable-streams-in-node-js/
  // https://community.risingstack.com/the-definitive-guide-to-object-streams-in-node-js/
  // when pushing data from outside the stream this is not needed nor used / a stub is required
};

PromiseRPCStream.prototype.validateId = function(id) {
  return !!this.requests[id];
};

PromiseRPCStream.prototype._write = function(buffer, enc, cb) {
  this.dispatch(buffer);
  cb();
};

module.exports = {
  PromiseRPCStream: PromiseRPCStream
};
