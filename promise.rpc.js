var PromiseRPCStream = require("./promise.rpc.stream").PromiseRPCStream;
var PromiseRPCProtocol = require("./promise.rpc.protocol").PromiseRPCProtocol;

module.exports = {
  PromiseRPCProtocol: PromiseRPCProtocol,
  PromiseRPCStream: PromiseRPCStream,
};
