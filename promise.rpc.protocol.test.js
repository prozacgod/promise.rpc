const { PromiseRPCProtocol } = require("./promise.rpc.protocol");

function delay(n) {
  return new Promise(resolve => setTimeout(resolve, n));
}

test("should not define global.window", function() {
  expect(global.window).toBeUndefined();
});

function connect(a, b) {
  a.onDispatch = msg => b.dispatch(msg);
  b.onDispatch = msg => a.dispatch(msg);
}

function debugConnect(a, b) {
  a.onDispatch = msg => {
    console.log("A -> B", JSON.stringify(msg));
    b.dispatch(msg);
  };
  b.onDispatch = msg => {
    console.log("B -> A", JSON.stringify(msg));
    a.dispatch(msg);
  };
}

describe("Promise RPC Protocol", () => {
  it("Can timeout requests", async () => {
    let handle;
    const a = new PromiseRPCProtocol({
      async test() {
        await new Promise((resolve, reject) => {
          handle = setTimeout(resolve, 100);
        });
        return "foo";
      }
    });

    const b = new PromiseRPCProtocol();
    //NOTE: timeout set to 10ms in here to keep test from being long
    b.TIMEOUT = 10;

    connect(a, b);

    const req = b.request("test");

    await expect(req).rejects.toEqual("timeout");
    // prevent unnecessary waiting.
    clearTimeout(handle);
  });

  it("Uses default router function", async () => {
    const fns = {
      test: jest.fn(() => {
        return "foo";
      })
    };

    const a = new PromiseRPCProtocol(fns);
    const b = new PromiseRPCProtocol();

    connect(a, b);

    b.execute("test", [1]);
    await b.request("test", [2]);
    expect(fns.test.mock.calls).toEqual([[1], [2]]);
  });

  it("Uses the router function", async () => {
    const fn = jest.fn(() => {
      return "foo";
    });

    const a = new PromiseRPCProtocol(fn);
    const b = new PromiseRPCProtocol();

    connect(a, b);

    b.execute("test", [1]);
    await b.request("test", [2]);

    const calls = fn.mock.calls;
    delete calls[1][0].id;

    expect(calls).toEqual([
      [
        {
          action: "execute",
          args: [1],
          name: "test"
        }
      ],

      [
        {
          action: "request",
          args: [2],
          name: "test"
        }
      ]
    ]);
  });

  it("Destruction forces rejection", async () => {
    /*
      NOTE: All handlers will eventually expire, you are not "required" to destroy the rpc.protocol
      It is a nice feature, if you call destroy it will cancel all outstanding requests.
    */
    let handle;
    const a = new PromiseRPCProtocol({
      async test() {
        await new Promise((resolve, reject) => {
          handle = setTimeout(resolve, 100);
        });
        return "foo";
      }
    });

    const b = new PromiseRPCProtocol();
    //NOTE: timeout set to 10ms in here to keep test from being long
    b.TIMEOUT = 10;

    connect(a, b);

    const req = b.request("test");

    b.destroy();

    await expect(req).rejects.toEqual("destroyed");
    // prevent unnecessary waiting.
    clearTimeout(handle);
  });

  it("Uses monkey-patched id function", async () => {
    // if you wish to use a different id function, that's allowed.  It must return a string value
    const a = new PromiseRPCProtocol();
    a.TIMEOUT = 5;

    let index = 0;
    a.randomId = jest.fn(() => {
      return `id_${index++}`;
    });

    a.onDispatch = jest.fn();

    await expect(a.request("foo", [1, 2, 3, 4])).rejects.toEqual("timeout");
    await expect(a.request("foo", ["a"])).rejects.toEqual("timeout");

    expect(a.onDispatch.mock.calls).toEqual([
      [{ action: "request", args: [1, 2, 3, 4], id: "id_0", name: "foo" }],
      [{ action: "request", args: ["a"], id: "id_1", name: "foo" }]
    ]);
  });

  it("can emit an execute packet", async () => {
    const a = new PromiseRPCProtocol();
    a.onDispatch = jest.fn();

    a.execute("foo", ["test"]);
    expect(a.onDispatch.mock.calls).toEqual([
      [
        {
          action: "execute",
          args: ["test"],
          name: "foo"
        }
      ]
    ]);
  });

  it("can parse an execute packet", async () => {
    let a = new PromiseRPCProtocol(jest.fn());

    a.dispatch({
      action: "execute",
      args: ["test"],
      name: "foo"
    });

    expect(a.handler.mock.calls).toEqual([
      [
        {
          action: "execute",
          args: ["test"],
          name: "foo"
        }
      ]
    ]);
  });

  it("can emit a request packet", async () => {
    const a = new PromiseRPCProtocol();
    a.randomId = () => "a";
    a.onDispatch = jest.fn();

    const req_result = a.request("foo", ["test"]);
    const calls = a.onDispatch.mock.calls;
    expect(calls.length).toBe(1);

    expect(calls).toEqual([
      [
        {
          action: "request",
          args: ["test"],
          name: "foo",
          id: "a"
        }
      ]
    ]);

    a.destroy();
    await expect(req_result).rejects.toEqual("destroyed");
  });

  it("Client can await a result from the server", async () => {
    const b = new PromiseRPCProtocol(req => {
      return Promise.resolve("Poop");
    });

    const a = new PromiseRPCProtocol();

    connect(a, b);

    const result = await a.request("thing", ["a", 1, 2]);

    expect(result).toBe("Poop");
  });

  it("Writes multiple packets", async () => {
    const b = new PromiseRPCProtocol();

    b.onDispatch = jest.fn();

    b.execute("foo", [1, 2, 3]);
    b.execute("bar", [1, 2, 3]);
    b.execute("baz", ["a", 2, 3]);
    b.execute("boop", [1, "b", 3]);
    b.execute("bang", [1, 2, "c"]);

    expect(b.onDispatch.mock.calls).toEqual([
      [{ action: "execute", args: [1, 2, 3], name: "foo" }],
      [{ action: "execute", args: [1, 2, 3], name: "bar" }],
      [{ action: "execute", args: ["a", 2, 3], name: "baz" }],
      [{ action: "execute", args: [1, "b", 3], name: "boop" }],
      [{ action: "execute", args: [1, 2, "c"], name: "bang" }]
    ]);
  });

  it("Handles multiple queued requests, and correct resolution", async () => {
    const a = new PromiseRPCProtocol();
    const b = new PromiseRPCProtocol({
      multiply(arg1, arg2) {
        return arg1 * arg2;
      },
      add(arg1, arg2) {
        return arg1 + arg2;
      }
    });

    connect(a, b);

    const resulta = a.request("multiply", [1234891, 19823982]);
    const resultb = a.request("add", [1234891, 19823982]);

    expect(Object.keys(a.requests).length).toBe(2);

    expect(await resulta).toBe(24480456955962);
    expect(await resultb).toBe(21058873);
  });
});
