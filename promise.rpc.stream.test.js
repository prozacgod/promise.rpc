const through2 = require("through2");
const { PromiseRPCStream } = require("./promise.rpc.stream");

const createObj2JsonStream = () => {
  return through2({ objectMode: true }, function(chunk, enc, callback) {
    this.push(JSON.stringify(chunk));
    callback();
  });
};

const createJson2ObjStream = () => {
  return through2({ objectMode: true }, function(chunk, enc, callback) {
    this.push(JSON.parse(chunk));
    callback();
  });
};

function delay(n) {
  return new Promise(resolve => setTimeout(resolve, n));
}

test("should not define global.window", function() {
  expect(global.window).toBeUndefined();
});

describe("JSON Stream RPC Protocol", () => {
  it("Can timeout requests", async () => {
    let handle;
    const a = new PromiseRPCStream({
      async test() {
        await new Promise((resolve, reject) => {
          handle = setTimeout(resolve, 100);
        });
        return "foo";
      }
    });

    const b = new PromiseRPCStream();
    //NOTE: timeout set to 10ms in here to keep test from being long
    b.TIMEOUT = 10;

    a.pipe(b);
    b.pipe(a);

    const req = b.request("test");

    await expect(req).rejects.toEqual("timeout");
    // prevent unnecessary waiting.
    clearTimeout(handle);
  });

  it("Uses default router function", async () => {
    const fns = {
      test: jest.fn(() => {
        return "foo";
      })
    };

    const a = new PromiseRPCStream(fns);
    const b = new PromiseRPCStream();

    a.pipe(b);
    b.pipe(a);

    b.execute("test", [1]);
    await b.request("test", [2]);

    expect(fns.test.mock.calls).toEqual([[1], [2]]);
  });

  it("Uses the router function", async () => {
    const fn = jest.fn(() => {
      return "foo";
    });

    const a = new PromiseRPCStream(fn);
    const b = new PromiseRPCStream();

    a.pipe(b);
    b.pipe(a);

    b.execute("test", [1]);
    await b.request("test", [2]);

    const calls = fn.mock.calls;
    delete calls[1][0].id;

    expect(calls).toEqual([
      [
        {
          action: "execute",
          args: [1],
          name: "test"
        }
      ],

      [
        {
          action: "request",
          args: [2],
          name: "test"
        }
      ]
    ]);
  });

  it("Is readable and writable", () => {
    const a = new PromiseRPCStream();
    expect(a.readable).toBeTruthy();
    expect(a.writable).toBeTruthy();
  });

  it("Destruction forces rejection", async () => {
    /*
            All handlers will eventuall expire, but if the user of the api signals the destroy function it will cancel all outstanding requests.
        */
    let handle;
    const a = new PromiseRPCStream({
      async test() {
        await new Promise((resolve, reject) => {
          handle = setTimeout(resolve, 100);
        });
        return "foo";
      }
    });

    const b = new PromiseRPCStream();
    //NOTE: timeout set to 10ms in here to keep test from being long
    b.TIMEOUT = 10;

    a.pipe(b);
    b.pipe(a);

    const req = b.request("test");

    b.destroy();

    await expect(req).rejects.toEqual("destroyed");
    // prevent unnecessary waiting.
    clearTimeout(handle);
  });

  it("Uses monkey-patched id function", async () => {
    // if you wish to use a different id function, that's allowed.  It must return a string value
    const a = new PromiseRPCStream();
    a.TIMEOUT = 5;

    let index = 0;
    a.randomId = jest.fn(() => {
      return `id_${index++}`;
    });

    const fn = jest.fn();
    a.on("data", fn);

    await expect(a.request("foo", [1, 2, 3, 4])).rejects.toEqual("timeout");
    await expect(a.request("foo", ["a"])).rejects.toEqual("timeout");

    expect(fn.mock.calls).toEqual([
      [{ action: "request", args: [1, 2, 3, 4], id: "id_0", name: "foo" }],
      [{ action: "request", args: ["a"], id: "id_1", name: "foo" }]
    ]);
  });

  it("can emit an execute packet", async () => {
    const a = new PromiseRPCStream();
    const result = new Promise((resolve, reject) =>
      a.on("data", d => resolve(d))
    );

    a.execute("foo", ["test"]);
    expect(await result).toEqual({
      action: "execute",
      args: ["test"],
      name: "foo"
    });
  });

  it("can parse an execute packet", async () => {
    let a;
    let req = new Promise(
      (resolve, reject) =>
        (a = new PromiseRPCStream(request => resolve(request)))
    );

    a.write({
      action: "execute",
      args: ["test"],
      name: "foo"
    });

    expect(await req).toEqual({
      action: "execute",
      args: ["test"],
      name: "foo"
    });
  });

  it("can emit a request packet", async () => {
    const a = new PromiseRPCStream();
    const result = new Promise((resolve, reject) =>
      a.on("data", d => resolve(d))
    );

    const req_result = a.request("foo", ["test"]);
    const r = await result;
    delete r.id;
    expect(r).toEqual({
      action: "request",
      args: ["test"],
      name: "foo"
    });

    a.destroy();
    await expect(req_result).rejects.toEqual("destroyed");
  });

  it("works as expected over a object->json through stream bridge", async () => {
    let a, b;

    let reqa = new Promise(
      (resolve, reject) =>
        (a = new PromiseRPCStream(request => resolve(request)))
    );
    let reqb = new Promise(
      (resolve, reject) =>
        (b = new PromiseRPCStream(request => resolve(request)))
    );

    const a_o2json = createObj2JsonStream();
    const a_json2o = createJson2ObjStream();

    const b_o2json = createObj2JsonStream();
    const b_json2o = createJson2ObjStream();

    a.pipe(a_o2json);
    a_o2json.pipe(a_json2o);
    a_json2o.pipe(b);

    b.pipe(b_o2json);
    b_o2json.pipe(b_json2o);
    b_json2o.pipe(a);

    a.execute("foo", ["test", { meh: 1234 }]);
    b.execute("bar", ["answer", { is: 42 }]);

    expect(await reqa).toEqual({
      action: "execute",
      args: ["answer", { is: 42 }],
      name: "bar"
    });
    expect(await reqb).toEqual({
      action: "execute",
      args: ["test", { meh: 1234 }],
      name: "foo"
    });
  });

  it("Client can await a result from the server", async () => {
    const b = new PromiseRPCStream(req => {
      return Promise.resolve("Poop");
    });

    const a = new PromiseRPCStream();

    a.pipe(b);
    b.pipe(a);

    const result = await a.request("thing", ["a", 1, 2]);

    expect(result).toBe("Poop");
  });

  it("Writes multiple packets", async () => {
    const b = new PromiseRPCStream();

    const handler = jest.fn();
    b.on("data", handler);

    b.execute("foo", [1, 2, 3]);

    // the callbacks for emit are queued for the next ... cycle so we just wait a moment.
    await delay(1);

    expect(handler.mock.calls).toEqual([
      [{ action: "execute", args: [1, 2, 3], name: "foo" }]
    ]);
  });

  it("Handles multiple queued requests, and correct resolution", async () => {
    const a = new PromiseRPCStream();
    const b = new PromiseRPCStream({
      multiply(arg1, arg2) {
        return arg1 * arg2;
      },
      add(arg1, arg2) {
        return arg1 + arg2;
      }
    });

    a.pipe(b);
    b.pipe(a);

    const resulta = a.request("multiply", [1234891, 19823982]);
    const resultb = a.request("add", [1234891, 19823982]);

    expect(Object.keys(a.requests).length).toBe(2);

    expect(await resulta).toBe(24480456955962);
    expect(await resultb).toBe(21058873);
  });
});
